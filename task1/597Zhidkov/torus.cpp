#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "TorusModel.hpp"

#include <iostream>
#include <vector>

/**
Куб и кролик. Управление виртуальной камерой. Вращение кролика.
*/


class SampleApplication : public Application
{
public:
    MeshPtr _cube;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        //Создаем меш с тором
        _cube = makeTorus(1.0f, 0.1f, N);
        _cube->setModelMatrix(glm::mat4(1.0f));

        //Создаем шейдерную программу        
        _shader = std::make_shared<ShaderProgram>("597ZhidkovData1/shader.vert", "597ZhidkovData1/shader.frag");
    }


    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
        _cube->draw();


    }

    void updateTorus(int dN) {
        N += dN;
        _cube = makeTorus(1.0f, 0.1f, N);
        _cube->setModelMatrix(glm::mat4(1.0f));
        draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateTorus(-1);
            } else if (key == GLFW_KEY_EQUAL) {
                updateTorus(1);
            }
        }
    }
private:
    unsigned int N = 10;
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}