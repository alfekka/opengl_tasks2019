#pragma once


#include "maze.hpp"

#include <Camera.hpp>
#include <GLFW/glfw3.h>


namespace mazekit {

class MazeCameraMover : public CameraMover
{
public:
	MazeCameraMover(mazekit::MazePtr maze);
	~MazeCameraMover() override {}

	void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
	void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
	void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
	void update(GLFWwindow* window, double dt) override;
	bool checkCollision(glm::vec2 pos);

private:
	glm::vec3 cameraPosition_;
	glm::quat cameraRotation_;

	double oldXPosition_;
	double oldYPosition_;

	mazekit::MazePtr maze_;
};

}