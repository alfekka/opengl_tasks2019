set(SRC_FILES
    ${PROJECT_SOURCE_DIR}/common/Application.cpp
	${PROJECT_SOURCE_DIR}/common/DebugOutput.cpp
    ${PROJECT_SOURCE_DIR}/common/Camera.cpp
    ${PROJECT_SOURCE_DIR}/common/Mesh.cpp
    ${PROJECT_SOURCE_DIR}/common/ShaderProgram.cpp
)

set(HEADER_FILES
		${PROJECT_SOURCE_DIR}/common/Application.hpp
		${PROJECT_SOURCE_DIR}/common/DebugOutput.h
		${PROJECT_SOURCE_DIR}/common/Camera.hpp
		${PROJECT_SOURCE_DIR}/common/Mesh.hpp
		${PROJECT_SOURCE_DIR}/common/ShaderProgram.hpp
		${PROJECT_SOURCE_DIR}/heigths_map/heights_map.h
		${PROJECT_SOURCE_DIR}/heigths_map/grid_terrain.h

)

set(SHADER_FILES
    shaders_heights_map/shader.vert
    shaders_heights_map/shader.frag
    shaders_heights_map/shaderTimeCoord.vert
    shaders_heights_map/shaderTimeCoord.frag
)

source_group("Shaders" FILES
    ${SHADER_FILES}
)

MAKE_SAMPLE(task1)

COPY_RESOURCE(shaders_heights_map)

add_dependencies(task1 shaders_heights_map)


install(DIRECTORY ${PROJECT_SOURCE_DIR}/heigths_map/shaders_heights_map DESTINATION ${CMAKE_INSTALL_PREFIX})
