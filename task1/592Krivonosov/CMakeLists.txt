
add_definitions(-D GLM_ENABLE_EXPERIMENTAL)
set(SRC_FILES
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/Application.hpp
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        Main.h
        Main.cpp
        )


MAKE_OPENGL_TASK(592Krivonosov 1 "${SRC_FILES}")


target_include_directories(592Krivonosov1 PUBLIC
        common)

