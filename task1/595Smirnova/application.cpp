#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include "perlin_noise.h"

#include <iostream>
#include <vector>



glm::vec3 normal(float x, float y, float z, float x_n, float y_n, float z_n) {
    glm::vec3 start = glm::vec3(x_n, y_n, z_n);
    glm::vec3 end = glm::vec3(x, y, z);
    return normalize(glm::vec3(start.x - end.x, start.y - end.y, start.z - end.z));
}

MeshPtr makeSurface(unsigned int N = 100, int size = 5, int h = 10)
{
    PerlinNoise pn(237);

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for( unsigned int i = 0; i < N * size; i++ )
    {
        for( unsigned int j = 0; j < N * size; j++ )
        {
            glm::vec3 x = glm::vec3((float)i / N * size, (float)j / N * size, pn.noise((float)i / N, (float)j / N, 0) * h);
            glm::vec3 y = glm::vec3((float)(i + 1) / N * size, (float)j / N * size, pn.noise((float)(i + 1) / N, (float)j / N, 0) * h);
            glm::vec3 z = glm::vec3((float)i / N * size, (float)(j + 1) / N * size, pn.noise((float)i / N, (float)(j + 1) / N, 0) * h);

            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(z);

            normals.push_back(glm::normalize(x));
            normals.push_back(glm::normalize(y));
            normals.push_back(glm::normalize(z));

            glm::vec3 a = glm::vec3((float)(i + 1) / N * size, (float)(j + 1) / N * size, pn.noise((float)(i + 1) / N, (float)(j + 1) / N, 0) * h);
            glm::vec3 b = glm::vec3((float)(i + 1) / N * size, (float)j / N * size, pn.noise((float)(i + 1) / N, (float)j / N, 0) * h);
            glm::vec3 c = glm::vec3((float)i / N * size, (float)(j + 1) / N * size, pn.noise((float)i / N, (float)(j + 1) / N, 0) * h);

            vertices.push_back(a);
            vertices.push_back(b);
            vertices.push_back(c);

            normals.push_back(glm::normalize(a));
            normals.push_back(glm::normalize(b));
            normals.push_back(glm::normalize(c));
       }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class SampleApplication : public Application
{
  public:
    MeshPtr _surface;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _surface = makeSurface();
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, -10.0f, -10.0f)));

        _shader = std::make_shared<ShaderProgram>("595SmirnovaData1/simple.vert", "595SmirnovaData1/simple.frag");
        _cameraMover = std::make_shared<FreeCameraMover>();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}