#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <vector>
#include <SOIL2.h>
#include <cmath>


void
addIndices(std::vector<unsigned int> &indices, int &p) {
    indices.push_back(p);
    indices.push_back(p+1);
    indices.push_back(p+2);
    indices.push_back(p+2);
    indices.push_back(p+3);
    indices.push_back(p+1);
    p += 4;
}

int
getHeightmapPoint(unsigned char *ht_map, int w, int x, int y) {
    return ht_map[x + y * w];
}

void
addNormal(std::vector<glm::vec3> *normal, unsigned char *ht_map, int width, int x, int z, float step) {
    float y1 = 0.0f;
    float y2 = 0.0f;
    float y3 = 0.0f;
    float y4 = 0.0f;

    if( x > 0 && z > 0 )
        y1 = getHeightmapPoint(ht_map, width, x-1, z-1) / 255.0f;
    y2 = getHeightmapPoint(ht_map, width, x+1, z+1) / 255.0f;
    if( x > 0 )
        y3 = getHeightmapPoint(ht_map, width, x-1, z+1) / 255.0f;
    if( z > 0 )
        y4 = getHeightmapPoint(ht_map, width, x+1, z-1) / 255.0f;

    float xpos = x * step;
    float zpos = z * step;

    glm::vec3 v1(xpos-step, zpos-step, y1);
    glm::vec3 v2(xpos+step, zpos+step, y2);
    glm::vec3 v3(xpos-step, zpos+step, y3);
    glm::vec3 v4(xpos+step, zpos-step, y4);

    glm::vec3 a = v1 - v2;
    glm::vec3 b = v3 - v4;

    glm::vec3 n1 = glm::normalize(glm::cross(a, b));

    normal->push_back(n1);
}
MeshPtr
makeGround(unsigned char *ht_map) {
    /* load an image as a heightmap, forcing greyscale (so channels should be 1) */
    int width, height, channels;
    ht_map = SOIL_load_image
        (
            "593YakimushkinaData1/terrain3.jpg",
            &width, &height, &channels,
            SOIL_LOAD_L
        );
    std::cout << width << " : " << height << " : "<< channels << std::endl;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normal;
    std::vector<glm::vec2> texcoords;
    std::vector<unsigned int> indices;

    float step = 0.1;
    int p = 0;

    for( int z = 0; z < width-1; z += 1 ) {
        for( int x = 0; x < height-1; x += 1 ) {
            // Вершины
            float y1 = getHeightmapPoint(ht_map, width, x, z) / 255.0f;
            float y2 = getHeightmapPoint(ht_map, width, x, z+1) / 255.0f;
            float y3 = getHeightmapPoint(ht_map, width, x+1, z) / 255.0f;
            float y4 = getHeightmapPoint(ht_map, width, x+1, z+1) / 255.0f;

            float xpos = x * step;
            float zpos = z * step;

            glm::vec3 v1(     xpos, zpos,      y1);
            glm::vec3 v2(     xpos, zpos+step, y2);
            glm::vec3 v3(xpos+step, zpos,      y3);
            glm::vec3 v4(xpos+step, zpos+step, y4);

            vertices.push_back(v1);
            vertices.push_back(v2);
            vertices.push_back(v3);
            vertices.push_back(v4);

            addNormal(&normal, ht_map, width,   x,   z, step);
            addNormal(&normal, ht_map, width,   x, z+1, step);
            addNormal(&normal, ht_map, width, x+1,   z, step);
            addNormal(&normal, ht_map, width, x+1, z+1, step);

            texcoords.push_back(glm::vec2(0.0, 0.0));
            texcoords.push_back(glm::vec2(0.0, 1.0));
            texcoords.push_back(glm::vec2(1.0, 0.0));
            texcoords.push_back(glm::vec2(1.0, 1.0));

            addIndices(indices, p);
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);

    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    buf1->setData(normal.size() * sizeof(float) * 3, normal.data());
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    DataBufferPtr buf_indices = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
    buf_indices->setData(indices.size() * sizeof(unsigned int), indices.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setIndices(indices.size(), buf_indices);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

class SampleApplication : public Application
{
public:
    MeshPtr _ground;

    ShaderProgramPtr _shader1;

    // Координаты источника света
    float _phi = 0.0f;
    float _theta = 0.0f;

    // Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;

    // Параметры материала
    glm::vec3 _groundAmbientColor;
    glm::vec3 _groundDiffuseColor;

    // Текстуры
    TexturePtr _grassTex;
    TexturePtr _gravelTex;
    TexturePtr _sandTex;
    TexturePtr _snowTex;
    TexturePtr _maskTex;

    GLuint _sampler;

    unsigned char *ht_map;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _ground = makeGround(ht_map);
        
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

        //Создаем шейдерную программу
        _shader1 = std::make_shared<ShaderProgram>("593YakimushkinaData1/terrain.vert", "593YakimushkinaData1/terrain.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _lightAmbientColor = glm::vec3(0.8, 0.8, 0.8);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);

        //Инициализация материала
        _groundAmbientColor = glm::vec3(1.0, 1.0, 1.0);
        _groundDiffuseColor = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _grassTex = loadTexture("593YakimushkinaData1/grass.jpg");
        _gravelTex = loadTexture("593YakimushkinaData1/gravel.jpg");
        _sandTex = loadTexture("593YakimushkinaData1/sand.jpg");
        _snowTex = loadTexture("593YakimushkinaData1/snow.jpg");
        _maskTex = loadTexture("593YakimushkinaData1/mask2.png");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();
        int x = int((_camera.pos.x) / 0.1f);
        int y = int((_camera.pos.y) / 0.1f);
        if( x < 0 ) x = 0;
        if( y < 0 ) y = 0;
        float h = getHeightmapPoint(ht_map, 512, x, y);
        Application::update(1);
        //std::cout << x << " : " << y << " : " << h / 255.0f + 1 << std::endl;

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));

                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();
        
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.2f,0.3f,0.4f,1.0f);

        //Устанавливаем шейдер.
        _shader1->use();

        //Устанавливаем общие юниформ-переменные
        _shader1->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader1->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        _shader1->setVec3Uniform("light.dir", lightDir);
        _shader1->setVec3Uniform("light.La", _lightAmbientColor);
        _shader1->setVec3Uniform("light.Ld", _lightDiffuseColor);

        _shader1->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        _shader1->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _gravelTex->bind();
        _shader1->setIntUniform("gravelTex", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindSampler(1, _sampler);
        _grassTex->bind();
        _shader1->setIntUniform("grassTex", 1);

        glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2
        glBindSampler(2, _sampler);
        _snowTex->bind();
        _shader1->setIntUniform("snowTex", 2);

        glActiveTexture(GL_TEXTURE3);  //текстурный юнит 3
        glBindSampler(3, _sampler);
        _sandTex->bind();
        _shader1->setIntUniform("sandTex", 3);

        glActiveTexture(GL_TEXTURE4);  //текстурный юнит 4
        glBindSampler(4, _sampler);
        _maskTex->bind();
        _shader1->setIntUniform("maskTex", 4);

        //Рисуем первый меш
        _shader1->setVec3Uniform("material.Ka", glm::vec3(1.0, 1.0, 1.0));
        _shader1->setVec3Uniform("material.Kd", glm::vec3(1.0, 1.0, 1.0));

        _ground->draw();
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        //_ground->draw();
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}