#version 330
uniform sampler2D gravelTex;
uniform sampler2D grassTex;
uniform sampler2D snowTex;
uniform sampler2D sandTex;
uniform sampler2D maskTex;

struct LightInfo
{
    vec3 dir; //направление на источник света в мировой системе координат (для направленного источника)
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
};
uniform LightInfo light;

struct MaterialInfo
{
    vec3 Ka; //коэффициент отражения окружающего света
    vec3 Kd; //коэффициент отражения диффузного света
};
uniform MaterialInfo material;

in vec3 color; //интерполированный цвет между вершинами треугольника
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec3 position; //текстурные координаты вершины
in float NdotL;

out vec4 fragColor; //выходной цвет фрагмента

void main()
{

    vec3 groundTexColor = texture(gravelTex, texCoord).rgb;
    vec3 grassTexColor  = texture(grassTex,  texCoord).rgb;
    vec3 snowTexColor  = texture(snowTex,  texCoord).rgb;
    vec3 sandTexColor  = texture(sandTex,  texCoord).rgb;
    vec4 mask  = texture(maskTex,  vec2(position.x / 512.0f / 0.1f, position.y / 512.0f / 0.1f)).rgba;
    vec3 texColor = mask.g * grassTexColor + mask.r * groundTexColor + mask.b * sandTexColor + (1 - mask.a) * snowTexColor;

    //vec3 color = light.La * material.Ka + light.Ld * material.Kd * NdotL; //цвет вершины
    float mkd = mask.g * 0.3 + mask.r * 0.1 + mask.b * 0.2 + (1 - mask.a) * 1;

    vec3 color = light.La * vec3(1) + light.Ld * vec3(mkd) * NdotL; //цвет вершины

    fragColor = vec4(texColor * color, 1.0f);
}
